﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace FilesSearcher
{
    public partial class MainForm : Form
    {
        private enum State
        {
            Ready,
            InProgress,
            Paused
        }

        private enum SearchEvent
        {
            StartSearch,
            Pause,
            Resume,
            Stop
        }

        State AppState;
        
        private const string StartButtonCaption = "🔍 Search";
        private const string PauseButtonCaption = "⏸ Pause";
        private const string ResumeButtonCaption = "🔁 Resume";

        private const string TimeFormat = "{0:mm'm':ss's '.' 'fff}";
        private const string PathSeparator = @"\\";
        
        private const string DefaultDirectory = @"..\..\..\TestFolder";
        private const string DefaultFilenamePattern = "*.*";
        private const string DefaultContent = "";

        private string actDirectory;
        private string actFilenamePattern;
        private byte[] actContent;
        private int actContentSize;

        private Thread searchThread;
        private ManualResetEvent manualResetEvent = new ManualResetEvent(true);
        
        private DateTime timeEllapsed;
        private DateTime previousTime;

        private int filesProcessed;
        private string currentFile;
        private int currentFileIndex;

        private IEnumerable<string> allFiles;
        private IEnumerable<string> filesFoundByMask;
        private List<string> fileMatchesFound;

        public MainForm()
        {
            InitializeComponent();

            ChangeAppState(State.Ready);

            ChooseFolderDialog.Description = "Choose folder to start the search from";
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadSettings();

            ChooseFolderDialog.SelectedPath = actDirectory;
            SearchFolderTextBox.Text = actDirectory;
            FilenamePatternTextBox.Text = actFilenamePattern;
        }

        private void LoadSettings()
        {
            string loadedDirectory = Properties.Settings.Default.LastDirectory;
            string loadedFilenamePattern = Properties.Settings.Default.LastFilenamePattern;
            string loadedTextContent = Properties.Settings.Default.LastTextContent;

            if (Directory.Exists(DefaultDirectory))
                Environment.CurrentDirectory = DefaultDirectory;
            if (Directory.Exists(loadedDirectory))
                Environment.CurrentDirectory = loadedDirectory;
            actDirectory = Environment.CurrentDirectory;
         
            actFilenamePattern = string.IsNullOrWhiteSpace(loadedFilenamePattern) ? DefaultFilenamePattern : loadedFilenamePattern;
        }

        private void FixateChosenDirectory(string path)
        {
            actDirectory = path;
            SearchFolderTextBox.Text = path;
        }

        private void SwitchPanelActiveness(bool isEnabled)
        {
            ChooseFolderButton.Enabled = isEnabled;
            SearchFolderTextBox.Enabled = isEnabled;
            FilenamePatternTextBox.Enabled = isEnabled;
            FileContentTextBox.Enabled = isEnabled;
        }

        private void ClearSearchButtonEventHandlers()
        {
            SearchButton.Click -= SearchButtonEventHandler(SearchEvent.StartSearch);
            SearchButton.Click -= SearchButtonEventHandler(SearchEvent.Pause);
            SearchButton.Click -= SearchButtonEventHandler(SearchEvent.Resume);
        }

        private EventHandler SearchButtonEventHandler(SearchEvent searchEvent)
        {
            switch (searchEvent)
            {
                case SearchEvent.StartSearch:
                    return (sender, EventArgs) => { AppEvent(SearchEvent.StartSearch); };
                case SearchEvent.Pause:
                    return (sender, EventArgs) => { AppEvent(SearchEvent.Pause); };
                case SearchEvent.Resume:
                    return (sender, EventArgs) => { AppEvent(SearchEvent.Resume); };
            }

            return null;
        }

        private void ChangeAppState(State state)
        {
            AppState = state;

            switch (state)
            {
                case State.Ready:
                    SearchButton.Text = StartButtonCaption;
                    ClearSearchButtonEventHandlers();
                    SearchButton.Click += SearchButtonEventHandler(SearchEvent.StartSearch);
                    StopButton.Enabled = false;
                    break;

                case State.InProgress:
                    SearchButton.Text = PauseButtonCaption;
                    ClearSearchButtonEventHandlers();
                    SearchButton.Click += SearchButtonEventHandler(SearchEvent.Pause);
                    StopButton.Enabled = true;
                    break;

                case State.Paused:
                    SearchButton.Text = ResumeButtonCaption;
                    ClearSearchButtonEventHandlers();
                    SearchButton.Click += SearchButtonEventHandler(SearchEvent.Resume);
                    break;
            }
        }

        private void AppEvent(SearchEvent searchEvent)
        {
            switch (searchEvent)
            {
                case SearchEvent.StartSearch:
                    SwitchPanelActiveness(false);
                    PrepareSearch();

                    ChangeAppState(State.InProgress);
                    Timer.Start();
                    searchThread = new Thread(() => PerformSearch());
                    searchThread.Start();
                    break;

                case SearchEvent.Pause:
                    ChangeAppState(State.Paused);
                    Timer.Stop();
                    manualResetEvent.Reset();                    
                    break;

                case SearchEvent.Resume:
                    previousTime = DateTime.Now;
                    previousTime.AddTicks(timeEllapsed.Ticks);
                    
                    ChangeAppState(State.InProgress);
                    Timer.Start();
                    manualResetEvent.Set();
                    break;

                case SearchEvent.Stop:
                    SwitchPanelActiveness(true);
                    CurrentFileValueLabel.Text = "";
                    StopButton.Enabled = false;

                    ChangeAppState(State.Ready);
                    Timer.Stop();
                    searchThread.Abort();
                    break;
            }
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            AppEvent(SearchEvent.Stop);
        }

        private void ChooseFolderButton_Click(object sender, EventArgs e)
        {
            if (ChooseFolderDialog.ShowDialog() == DialogResult.OK)
                FixateChosenDirectory(ChooseFolderDialog.SelectedPath);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (searchThread != null)
                searchThread.Abort();

            Properties.Settings.Default.Save();
        }
    }
}