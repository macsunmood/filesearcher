﻿namespace FilesSearcher
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.SearchButton = new System.Windows.Forms.Button();
            this.FilenamePatternLabel = new System.Windows.Forms.Label();
            this.FilenamePatternTextBox = new System.Windows.Forms.TextBox();
            this.FileContentLabel = new System.Windows.Forms.Label();
            this.FileContentTextBox = new System.Windows.Forms.TextBox();
            this.ProgressLabel = new System.Windows.Forms.Label();
            this.ChooseFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.CurrentFileCaptionText = new System.Windows.Forms.Label();
            this.CurrentFileValueLabel = new System.Windows.Forms.Label();
            this.FilesProcessedLabel = new System.Windows.Forms.Label();
            this.FilesProcessedValueLabel = new System.Windows.Forms.Label();
            this.TimeElapsedLabel = new System.Windows.Forms.Label();
            this.TimeElapsedValueLabel = new System.Windows.Forms.Label();
            this.SearchFolderLabel = new System.Windows.Forms.Label();
            this.SearchFolderTextBox = new System.Windows.Forms.TextBox();
            this.ResultsTreeView = new System.Windows.Forms.TreeView();
            this.ImageList = new System.Windows.Forms.ImageList(this.components);
            this.ChooseFolderButton = new System.Windows.Forms.Button();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.ResultsLabel = new System.Windows.Forms.Label();
            this.SluggingCheckBox = new System.Windows.Forms.CheckBox();
            this.SluggingNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.StopButton = new System.Windows.Forms.Button();
            this.MatchesValueLabel = new System.Windows.Forms.Label();
            this.MatchesLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SluggingNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // SearchButton
            // 
            this.SearchButton.AutoEllipsis = true;
            resources.ApplyResources(this.SearchButton, "SearchButton");
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.UseVisualStyleBackColor = true;
            // 
            // FilenamePatternLabel
            // 
            resources.ApplyResources(this.FilenamePatternLabel, "FilenamePatternLabel");
            this.FilenamePatternLabel.Name = "FilenamePatternLabel";
            // 
            // FilenamePatternTextBox
            // 
            this.FilenamePatternTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FilesSearcher.Properties.Settings.Default, "LastFilenamePattern", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.FilenamePatternTextBox, "FilenamePatternTextBox");
            this.FilenamePatternTextBox.Name = "FilenamePatternTextBox";
            this.FilenamePatternTextBox.Text = global::FilesSearcher.Properties.Settings.Default.LastFilenamePattern;
            // 
            // FileContentLabel
            // 
            resources.ApplyResources(this.FileContentLabel, "FileContentLabel");
            this.FileContentLabel.BackColor = System.Drawing.Color.Transparent;
            this.FileContentLabel.Name = "FileContentLabel";
            // 
            // FileContentTextBox
            // 
            this.FileContentTextBox.AcceptsReturn = true;
            this.FileContentTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FilesSearcher.Properties.Settings.Default, "LastTextContent", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.FileContentTextBox, "FileContentTextBox");
            this.FileContentTextBox.Name = "FileContentTextBox";
            this.FileContentTextBox.Text = global::FilesSearcher.Properties.Settings.Default.LastTextContent;
            // 
            // ProgressLabel
            // 
            this.ProgressLabel.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.ProgressLabel, "ProgressLabel");
            this.ProgressLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.ProgressLabel.Name = "ProgressLabel";
            // 
            // CurrentFileCaptionText
            // 
            resources.ApplyResources(this.CurrentFileCaptionText, "CurrentFileCaptionText");
            this.CurrentFileCaptionText.Name = "CurrentFileCaptionText";
            // 
            // CurrentFileValueLabel
            // 
            this.CurrentFileValueLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.CurrentFileValueLabel, "CurrentFileValueLabel");
            this.CurrentFileValueLabel.ForeColor = System.Drawing.Color.DarkRed;
            this.CurrentFileValueLabel.Name = "CurrentFileValueLabel";
            // 
            // FilesProcessedLabel
            // 
            resources.ApplyResources(this.FilesProcessedLabel, "FilesProcessedLabel");
            this.FilesProcessedLabel.Name = "FilesProcessedLabel";
            // 
            // FilesProcessedValueLabel
            // 
            this.FilesProcessedValueLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.FilesProcessedValueLabel, "FilesProcessedValueLabel");
            this.FilesProcessedValueLabel.ForeColor = System.Drawing.Color.Green;
            this.FilesProcessedValueLabel.Name = "FilesProcessedValueLabel";
            // 
            // TimeElapsedLabel
            // 
            resources.ApplyResources(this.TimeElapsedLabel, "TimeElapsedLabel");
            this.TimeElapsedLabel.Name = "TimeElapsedLabel";
            // 
            // TimeElapsedValueLabel
            // 
            this.TimeElapsedValueLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.TimeElapsedValueLabel, "TimeElapsedValueLabel");
            this.TimeElapsedValueLabel.ForeColor = System.Drawing.Color.Navy;
            this.TimeElapsedValueLabel.Name = "TimeElapsedValueLabel";
            // 
            // SearchFolderLabel
            // 
            resources.ApplyResources(this.SearchFolderLabel, "SearchFolderLabel");
            this.SearchFolderLabel.Name = "SearchFolderLabel";
            // 
            // SearchFolderTextBox
            // 
            this.SearchFolderTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SearchFolderTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FilesSearcher.Properties.Settings.Default, "LastDirectory", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.SearchFolderTextBox, "SearchFolderTextBox");
            this.SearchFolderTextBox.Name = "SearchFolderTextBox";
            this.SearchFolderTextBox.ReadOnly = true;
            this.SearchFolderTextBox.Text = global::FilesSearcher.Properties.Settings.Default.LastDirectory;
            // 
            // ResultsTreeView
            // 
            this.ResultsTreeView.BackColor = System.Drawing.SystemColors.ButtonFace;
            resources.ApplyResources(this.ResultsTreeView, "ResultsTreeView");
            this.ResultsTreeView.ImageList = this.ImageList;
            this.ResultsTreeView.Name = "ResultsTreeView";
            // 
            // ImageList
            // 
            this.ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList.ImageStream")));
            this.ImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList.Images.SetKeyName(0, "folder_16x16.png");
            this.ImageList.Images.SetKeyName(1, "file_16x16.png");
            // 
            // ChooseFolderButton
            // 
            this.ChooseFolderButton.BackColor = System.Drawing.Color.LemonChiffon;
            resources.ApplyResources(this.ChooseFolderButton, "ChooseFolderButton");
            this.ChooseFolderButton.Name = "ChooseFolderButton";
            this.ChooseFolderButton.UseVisualStyleBackColor = false;
            this.ChooseFolderButton.Click += new System.EventHandler(this.ChooseFolderButton_Click);
            // 
            // Timer
            // 
            this.Timer.Interval = 10;
            this.Timer.Tick += new System.EventHandler(this.UpdateElapsedTime);
            // 
            // ResultsLabel
            // 
            this.ResultsLabel.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.ResultsLabel, "ResultsLabel");
            this.ResultsLabel.ForeColor = System.Drawing.Color.Green;
            this.ResultsLabel.Name = "ResultsLabel";
            // 
            // SluggingCheckBox
            // 
            resources.ApplyResources(this.SluggingCheckBox, "SluggingCheckBox");
            this.SluggingCheckBox.Name = "SluggingCheckBox";
            this.SluggingCheckBox.UseVisualStyleBackColor = true;
            // 
            // SluggingNumericUpDown
            // 
            this.SluggingNumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            resources.ApplyResources(this.SluggingNumericUpDown, "SluggingNumericUpDown");
            this.SluggingNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.SluggingNumericUpDown.Name = "SluggingNumericUpDown";
            this.SluggingNumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // StopButton
            // 
            resources.ApplyResources(this.StopButton, "StopButton");
            this.StopButton.Name = "StopButton";
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // MatchesValueLabel
            // 
            this.MatchesValueLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.MatchesValueLabel, "MatchesValueLabel");
            this.MatchesValueLabel.ForeColor = System.Drawing.Color.DarkRed;
            this.MatchesValueLabel.Name = "MatchesValueLabel";
            // 
            // MatchesLabel
            // 
            resources.ApplyResources(this.MatchesLabel, "MatchesLabel");
            this.MatchesLabel.Name = "MatchesLabel";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MatchesValueLabel);
            this.Controls.Add(this.MatchesLabel);
            this.Controls.Add(this.StopButton);
            this.Controls.Add(this.SluggingNumericUpDown);
            this.Controls.Add(this.SluggingCheckBox);
            this.Controls.Add(this.ResultsLabel);
            this.Controls.Add(this.ChooseFolderButton);
            this.Controls.Add(this.ResultsTreeView);
            this.Controls.Add(this.SearchFolderTextBox);
            this.Controls.Add(this.SearchFolderLabel);
            this.Controls.Add(this.TimeElapsedValueLabel);
            this.Controls.Add(this.TimeElapsedLabel);
            this.Controls.Add(this.FilesProcessedValueLabel);
            this.Controls.Add(this.FilesProcessedLabel);
            this.Controls.Add(this.CurrentFileValueLabel);
            this.Controls.Add(this.CurrentFileCaptionText);
            this.Controls.Add(this.ProgressLabel);
            this.Controls.Add(this.FileContentTextBox);
            this.Controls.Add(this.FileContentLabel);
            this.Controls.Add(this.FilenamePatternTextBox);
            this.Controls.Add(this.FilenamePatternLabel);
            this.Controls.Add(this.SearchButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SluggingNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Label FilenamePatternLabel;
        private System.Windows.Forms.TextBox FilenamePatternTextBox;
        private System.Windows.Forms.Label FileContentLabel;
        private System.Windows.Forms.TextBox FileContentTextBox;
        private System.Windows.Forms.Label ProgressLabel;
        private System.Windows.Forms.FolderBrowserDialog ChooseFolderDialog;
        private System.Windows.Forms.Label CurrentFileCaptionText;
        private System.Windows.Forms.Label CurrentFileValueLabel;
        private System.Windows.Forms.Label FilesProcessedLabel;
        private System.Windows.Forms.Label FilesProcessedValueLabel;
        private System.Windows.Forms.Label TimeElapsedLabel;
        private System.Windows.Forms.Label TimeElapsedValueLabel;
        private System.Windows.Forms.Label SearchFolderLabel;
        private System.Windows.Forms.TextBox SearchFolderTextBox;
        private System.Windows.Forms.TreeView ResultsTreeView;
        private System.Windows.Forms.Button ChooseFolderButton;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.Label ResultsLabel;
        private System.Windows.Forms.CheckBox SluggingCheckBox;
        private System.Windows.Forms.NumericUpDown SluggingNumericUpDown;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.Label MatchesValueLabel;
        private System.Windows.Forms.Label MatchesLabel;
        private System.Windows.Forms.ImageList ImageList;
    }
}

