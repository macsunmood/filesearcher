﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Threading;

namespace FilesSearcher
{
    public partial class MainForm
    {
        private void PrepareSearch()
        {
            filesProcessed = 0;
            currentFileIndex = 0;
            timeEllapsed = new DateTime();
            previousTime = DateTime.Now;
            allFiles = Directory.EnumerateFiles(actDirectory, "*.*", SearchOption.AllDirectories);
            filesFoundByMask = FindFilesByMask();
            fileMatchesFound = new List<string>();

            FilesProcessedValueLabel.Text = "0";
            TimeElapsedValueLabel.Text = string.Format(TimeFormat, timeEllapsed);
            MatchesValueLabel.Text = "0";

            actContent = FileContentTextBox.Text.Length == 0 
                ? null : Encoding.UTF8.GetBytes(FileContentTextBox.Text);
            if (actContent != null)
                actContentSize = actContent.Length;

            ResultsTreeView.Nodes.Clear();
        }

        private IEnumerable<string> FindFilesByMask()
        {
            if (string.IsNullOrWhiteSpace(FilenamePatternTextBox.Text))
            {
                FilenamePatternTextBox.Text = DefaultFilenamePattern;
                actFilenamePattern = DefaultFilenamePattern;
            }
            else
                actFilenamePattern = FilenamePatternTextBox.Text;

            return
                Directory.EnumerateFiles(actDirectory, actFilenamePattern, SearchOption.AllDirectories);
        }

        public void UpdateElapsedTime(object sender, EventArgs e)
        {
            timeEllapsed = timeEllapsed.AddTicks(DateTime.Now.Ticks - previousTime.Ticks);
            previousTime = DateTime.Now;

            TimeElapsedValueLabel.Text = string.Format(TimeFormat, timeEllapsed);
        }

        private void CreateTreeViewBranch(string path)
        {
            IEnumerable<string> paths = path.Split(PathSeparator.ToCharArray());

            TreeNode lastNode = null;
            string subPathAgg = "";

            foreach (string subPath in paths)
            {
                subPathAgg += subPath + PathSeparator;

                TreeNode[] nodes = ResultsTreeView.Nodes.Find(subPathAgg, true);

                if (nodes.Length == 0)
                {
                    if (lastNode == null)
                        lastNode = ResultsTreeView.Nodes.Add(subPathAgg, Path.GetFileName(actDirectory));

                    else
                    {
                        if (subPath == paths.Last())
                            lastNode.Nodes.Add(new TreeNode(subPath, 1, 1));
                        else
                            lastNode = lastNode.Nodes.Add(subPathAgg, subPath);
                    }
                }
                else
                    lastNode = nodes[0];

                lastNode.Expand();
                ResultsTreeView.ExpandAll();
            }
        }

        private void PerformSearch()
        {
            while (currentFileIndex < allFiles.Count())
            {
                if (SluggingCheckBox.Checked)
                    Thread.Sleep((int)SluggingNumericUpDown.Value);

                currentFile = allFiles.ElementAt(currentFileIndex);
                if (AppState == State.InProgress)
                    BeginInvoke(new Action(() => { CurrentFileValueLabel.Text = currentFile; }));

                if (AppState == State.Paused)
                    manualResetEvent.WaitOne();

                if (filesFoundByMask.Contains(currentFile) 
                    & FileContainsSequence(currentFile))
                {
                    fileMatchesFound.Add(currentFile);

                    BeginInvoke(new Action(() => { CreateTreeViewBranch(currentFile.Replace(actDirectory, "")); }));
                }

                currentFileIndex++;
                filesProcessed++;

                BeginInvoke(new Action(() =>
                {
                    FilesProcessedValueLabel.Text = filesProcessed.ToString();
                    MatchesValueLabel.Text = fileMatchesFound.Count.ToString();
                }));
            }

            BeginInvoke(new Action(() => { AppEvent(SearchEvent.Stop); }));

            MessageBox.Show("Search completed !", "File Searcher");
        }


        private bool FileContainsSequence(string filePath)
        {
            if (actContent == null)
                return true;

            var file = File.OpenRead(filePath);

            Queue<byte> queue = new Queue<byte>(actContentSize);
            
            for (int b = file.ReadByte(); b != -1; b = file.ReadByte())
            {
                if (AppState == State.Paused)
                    manualResetEvent.WaitOne();

                if (queue.Count >= actContentSize)
                    queue.Dequeue();
                    
                queue.Enqueue((byte)b);

                if (actContent.Last().Equals((byte)b))
                    if (SequenceMatches(queue))
                        return true;
            }

            return false;
        }

        private bool SequenceMatches(Queue<byte> queue)
        {
            var maxLen = actContent.Length > queue.Count 
                       ? actContent.Length : queue.Count;

            var qList = queue.GetEnumerator();
            var cList = actContent.GetEnumerator();

            for (int i = 0; i < maxLen; i++)
            {
                if (qList.MoveNext() == false
                || cList.MoveNext() == false)
                    return false;

                if (!qList.Current.Equals(cList.Current))
                    return false;
            }

            return true;
        }        
    }
}